import { NextFunction, Response } from 'express';
import BaseRequest from 'modules/base/base.request';
import { Constants } from 'utils/constants';
import { throwError } from 'utils/http-exception';

export const tokenRequire = async (req: BaseRequest, _: Response, next: NextFunction) => {
  const token = (req.header(Constants.ACCESS_TOKEN_HQ) || req.query[Constants.ACCESS_TOKEN_HQ]) as string;
  if (!token) {
    throwError({
      statusCode: 400,
      message: 'No token send.',
    });
  }

  const index = Constants.TOKEN_LIST.findIndex((el) => el === token);

  if (index === -1) {
    throwError({
      statusCode: 401,
      message: 'Token incorrect',
    });
  }

  req.accessToken = token;

  next();
};
