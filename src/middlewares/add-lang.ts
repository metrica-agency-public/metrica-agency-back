import { NextFunction, Response } from 'express';
import BaseRequest from 'modules/base/base.request';
import { Constants, Lang } from 'utils/constants';
import { throwError } from 'utils/http-exception';

export const addLang = async (req: BaseRequest, _: Response, next: NextFunction) => {
  const currentLang = (req.header(Constants.X_CURRENT_LANG_HEADER_NAME) ||
    req.query[Constants.X_CURRENT_LANG_QUERY_NAME]) as Lang;

  if (!currentLang) {
    throwError({
      statusCode: 400,
      message: 'No lang sent.',
    });
  }

  req.currentLang = currentLang;

  next();
};
