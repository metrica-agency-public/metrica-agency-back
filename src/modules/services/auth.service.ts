import { LoginDto } from 'modules/dto/login.dto';
import { nanoid } from 'nanoid';
import { Constants } from 'utils/constants';
import { throwError } from 'utils/http-exception';
import UtilsENVConfig from 'utils/utils-env-config';

export default class AuthService {
  static async login(dto: LoginDto) {
    const loginIndex = UtilsENVConfig.getProcessEnv()
      .ADMIN_LOGIN_LIST.split(',')
      .findIndex((el) => el === dto.login);

    const passwordIndex = UtilsENVConfig.getProcessEnv()
      .ADMIN_PASSWORD_LIST.split(',')
      .findIndex((el) => el === dto.password);

    if (loginIndex === -1 || passwordIndex === -1) {
      throwError({
        message: 'Login or password is incorrect.',
      });
    }

    const accessToken = nanoid(Constants.HEADER_ACCESS_TOKEN_LENGTH);

    Constants.TOKEN_LIST.push(accessToken);

    return {
      accessToken,
    };
  }

  static async logout(accessToken: string) {
    const index = Constants.TOKEN_LIST.findIndex((el) => el === accessToken);

    if (index !== -1) {
      Constants.TOKEN_LIST.splice(index, 1);
    }

    return {
      message: 'Ok',
    };
  }
}
