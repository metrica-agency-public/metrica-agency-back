import ProjectStack from 'database/models/relations/project-stack.model';
import { ProjectStackCreateDto } from 'modules/dto/project-stack-create.dto';
import { ProjectStackUpdateDto } from 'modules/dto/project-stack-update.dto';
import SAProjectsService from './sa-projects.service';

export default class SAProjectStackService {
  static async add(dto: ProjectStackCreateDto) {
    await ProjectStack.create({ ...dto });

    return await SAProjectsService.getById(dto.projectId);
  }

  static async updateById(dto: ProjectStackUpdateDto) {
    await ProjectStack.update(
      {
        serialNumber: dto.serialNumber,
      },
      {
        where: {
          projectId: dto.projectId,
          stackId: dto.stackId,
        },
      }
    );

    return await SAProjectsService.getById(dto.projectId);
  }

  static async deleteById(projectId: string, stackId: string) {
    await ProjectStack.destroy({
      where: {
        stackId,
        projectId,
      },
    });

    return await SAProjectsService.getById(projectId);
  }
}
