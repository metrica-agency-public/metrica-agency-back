import Contact from 'database/models/final/contact.model';
import { MemberContactCreateDto } from 'modules/dto/member-contact-create.dto';
import { MemberContactUpdateDto } from 'modules/dto/member-contact-update.dto';
import SAMembersService from './sa-member.service';

export default class SAMembersContactService {
  static async create(dto: MemberContactCreateDto) {
    await Contact.create({ ...dto });

    return await SAMembersService.getById(dto.memberId);
  }

  static async updateById(dto: MemberContactUpdateDto) {
    await Contact.update(
      { ...dto },
      {
        where: {
          id: dto.contactId,
          memberId: dto.memberId,
        },
      }
    );
    return await SAMembersService.getById(dto.memberId);
  }

  static async deleteById(memberId: string, id: string) {
    await Contact.destroy({
      where: {
        id,
        memberId,
      },
    });

    return await SAMembersService.getById(memberId);
  }
}
