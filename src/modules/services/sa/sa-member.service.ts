import Contact from 'database/models/final/contact.model';
import FileDB from 'database/models/final/file-db.model';
import Member from 'database/models/final/member.model';
import Stack from 'database/models/final/stack.model';
import { MemberCreateDto } from 'modules/dto/members-create.dto';
import { MemberUpdateDto } from 'modules/dto/members-update.dto';
import { ParsedQs } from 'qs';
import { Op, literal, WhereOptions } from 'sequelize';
import SwaggerUtils from 'swagger/swagger-utils';
import { QueryShowType } from 'utils/constants';
import { throwError } from 'utils/http-exception';

export default class SAMembersService {
  static async getAll(query: ParsedQs) {
    const membersWhere: WhereOptions = {
      ...(query.show
        ? query.show === QueryShowType.ALL
          ? {}
          : query.show === QueryShowType.EXISTING
          ? { isExist: true }
          : { isExist: false }
        : {}),
      ...(query.searchValue
        ? {
            [Op.or]: [
              {
                firstName_ru: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                firstName_en: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                lastName_ru: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                lastName_en: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                about_ru: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                about_en: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                skills_ru: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                skills_en: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
            ],
          }
        : {}),
    };
    const memberList = await Member.findAll({
      where: membersWhere,
      include: [
        {
          model: Contact,
          duplicating: false,
          separate: true,
          order: [['serialNumber', 'ASC']],
        },
        {
          model: Stack,
          through: {
            attributes: ['serialNumber'],
          },
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'fullImage',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'previewImage',
          duplicating: false,
        },
      ],
      order: [
        ['serialNumber', 'ASC'],
        [literal(`"stackList.MemberStack.serialNumber"`), 'ASC'],
      ],
    });
    return {
      memberList,
    };
  }

  static async getById(id: string) {
    const member = await Member.findByPk(id, {
      include: [
        {
          model: FileDB,
          as: 'fullImage',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'previewImage',
          duplicating: false,
        },
        {
          model: Contact,
          duplicating: false,
        },
        {
          model: Stack,
          through: {
            attributes: ['serialNumber'],
          },
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
          duplicating: false,
        },
      ],
      order: [
        [literal(`"stackList.MemberStack.serialNumber"`), 'ASC'],
      ],
    });

    if (!member) {
      throwError({
        statusCode: 404,
        message: 'No member found.',
      });
    }

    return member;
  }

  static async searchMember(searchValue: string) {
    const memberList = await Member.findAll({
      where: {
        firstName: {
          [Op.iLike]: `%${searchValue}%`,
        },
      },
    });
    return memberList;
  }

  static async create(dto: MemberCreateDto) {
    let member = await Member.create({
      ...dto,
    });

    return await SAMembersService.getById(member.id);
  }

  static async updateById(dto: MemberUpdateDto) {
    const member = await SAMembersService.getById(dto.memberId);

    await member.update({
      ...dto,
    });

    return await SAMembersService.getById(dto.memberId);
  }

  static async deleteById(id: string) {
    let member = await SAMembersService.getById(id);

    await member.update({
      isExist: false,
    });

    return await SAMembersService.getById(id);
  }

  static async deleteForeverById(id: string) {
    let member = await SAMembersService.getById(id);

    await member.destroy();

    return SwaggerUtils.messageOk();
  }

  static async restoreById(id: string) {
    let member = await SAMembersService.getById(id);

    await member.update({
      isExist: true,
    });

    return await SAMembersService.getById(id);
  }
}
