import FileDB from 'database/models/final/file-db.model';
import Service from 'database/models/final/service.model';
import { ServicesCreateDto } from 'modules/dto/services-create.dto';
import { ServicesUpdateDto } from 'modules/dto/services-update.dto';
import { ParsedQs } from 'qs';
import { Op, WhereOptions } from 'sequelize';
import SwaggerUtils from 'swagger/swagger-utils';
import { QueryShowType } from 'utils/constants';
import { throwError } from 'utils/http-exception';

export default class SAServicesService {
  static async getAll(query: ParsedQs) {
    const servicesWhere: WhereOptions = {
      ...(query.show
        ? query.show === QueryShowType.ALL
          ? {}
          : query.show === QueryShowType.EXISTING
          ? { isExist: true }
          : { isExist: false }
        : {}),
      ...(query.searchValue
        ? {
            [Op.or]: [
              {
                title_ru: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                title_en: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
            ],
          }
        : {}),
    };

    const serviceList = await Service.findAll({
      where: servicesWhere,
      include: [
        {
          model: FileDB,
          duplicating: false,
        },
      ],
      order: [['serialNumber', 'ASC']],
    });

    return {
      serviceList,
    };
  }

  static async getById(id: string) {
    let service = await Service.findByPk(id, {
      include: [
        {
          model: FileDB,
          duplicating: false,
        },
      ],
    });

    if (!service) {
      throwError({
        statusCode: 404,
        message: 'No service found.',
      });
    }

    return service;
  }

  static async create(dto: ServicesCreateDto) {
    const service = await Service.create({ ...dto });

    return await SAServicesService.getById(service.id);
  }

  static async updateById(dto: ServicesUpdateDto) {
    let service = await SAServicesService.getById(dto.serviceId);

    await service.update(dto);

    return await SAServicesService.getById(dto.serviceId);
  }

  static async restoreById(id: string) {
    let service = await SAServicesService.getById(id);

    await service.update({
      isExist: true,
    });

    return await SAServicesService.getById(id);
  }

  static async deleteById(id: string) {
    let service = await SAServicesService.getById(id);

    await service.update({
      isExist: false,
    });

    return await SAServicesService.getById(id);
  }

  static async deleteForeverById(id: string) {
    let service = await SAServicesService.getById(id);

    await service.destroy();

    return SwaggerUtils.messageOk();
  }
}
