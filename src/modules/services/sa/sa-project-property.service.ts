import ProjectProperty from 'database/models/final/project-property.model';
import { ProjectPropertyCreateDto } from 'modules/dto/project-property-create.dto';
import { ProjectPropertyUpdateDto } from 'modules/dto/project-property-update.dto';
import SAProjectsService from './sa-projects.service';

export default class SAProjectPropertyService {
  static async create(dto: ProjectPropertyCreateDto) {
    await ProjectProperty.create({
      ...dto,
      projectId: dto.projectId,
    });

    return await SAProjectsService.getById(dto.projectId);
  }

  static async updateById(dto: ProjectPropertyUpdateDto) {
    await ProjectProperty.update(
      { ...dto },
      {
        where: {
          id: dto.propertyId,
          projectId: dto.projectId,
        },
      }
    );

    return await SAProjectsService.getById(dto.projectId);
  }

  static async deleteById(projectId: string, propertyId: string) {
    await ProjectProperty.destroy({
      where: {
        id: propertyId,
        projectId,
      },
    });

    return await SAProjectsService.getById(projectId);
  }
}
