import MemberStack from 'database/models/relations/member-stack.model';
import { MemberStackCreateDto } from 'modules/dto/member-stack-create.dto';
import { MemberStackUpdateDto } from 'modules/dto/member-stack-update.dto';
import SAMembersService from './sa-member.service';

export default class SAMembersStackService {
  static async add(dto: MemberStackCreateDto) {
    await MemberStack.create({ ...dto });

    return await SAMembersService.getById(dto.memberId);
  }

  static async updateById(dto: MemberStackUpdateDto) {
    await MemberStack.update(
      {
        serialNumber: dto.serialNumber,
      },
      {
        where: {
          memberId: dto.memberId,
          stackId: dto.stackId,
        },
      }
    );
    return await SAMembersService.getById(dto.memberId);
  }

  static async deleteById(memberId: string, stackId: string) {
    await MemberStack.destroy({
      where: {
        stackId,
        memberId,
      },
    });

    return await SAMembersService.getById(memberId);
  }
}
