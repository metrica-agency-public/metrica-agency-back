import MainPageMetaInfo from 'database/models/final/main-page-meta-info.model';
import { MetaInfoCreateDto } from 'modules/dto/meta-info-create.dto';
import { MetaInfoUpdateDto } from 'modules/dto/meta-info-update.dto';
import { Op } from 'sequelize';
import SwaggerUtils from 'swagger/swagger-utils';
import { throwError } from 'utils/http-exception';

export default class SAMetaInfoService {
  static async getAll(searchValue: string = '') {
    const metaInfoList = await MainPageMetaInfo.findAll({
      where: {
        [Op.or]: [
          {
            title_ru: {
              [Op.iLike]: `%${searchValue}%`,
            },
          },
          {
            title_en: {
              [Op.iLike]: `%${searchValue}%`,
            },
          },
          {
            description_ru: {
              [Op.iLike]: `%${searchValue}%`,
            },
          },
          {
            description_en: {
              [Op.iLike]: `%${searchValue}%`,
            },
          },
        ],
      },
      order: [['serialNumber', 'ASC']],
    });

    return {
      metaInfoList,
    };
  }

  static async create(dto: MetaInfoCreateDto) {
    const metaInfo = await MainPageMetaInfo.create({ ...dto });
    return metaInfo;
  }

  static async updateById(dto: MetaInfoUpdateDto) {
    let metaInfo = await MainPageMetaInfo.findByPk(dto.id);
    if (!metaInfo) {
      throwError({
        statusCode: 404,
        message: 'No meta info found.',
      });
    }
    await metaInfo.update(dto);

    return (await MainPageMetaInfo.findByPk(dto.id)) as MainPageMetaInfo;
  }

  static async deleteById(id: string) {
    let metaInfo = await MainPageMetaInfo.findByPk(id);
    if (!metaInfo) {
      throwError({
        statusCode: 404,
        message: 'No meta info found.',
      });
    }
    await metaInfo.destroy();

    return SwaggerUtils.messageOk();
  }
}
