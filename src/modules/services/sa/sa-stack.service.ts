import FileDB from 'database/models/final/file-db.model';
import Stack from 'database/models/final/stack.model';
import { StackCreateDto } from 'modules/dto/stack-create.dto';
import { StackUpdateDto } from 'modules/dto/stack-update.dto';
import { Op } from 'sequelize';
import SwaggerUtils from 'swagger/swagger-utils';
import { throwError } from 'utils/http-exception';

export default class SAStackService {
  static async getAll(searchValue: string = '') {
    const stackList = await Stack.findAll({
      where: {
        [Op.or]: [
          {
            title_ru: {
              [Op.iLike]: `%${searchValue}%`,
            },
          },
          {
            title_en: {
              [Op.iLike]: `%${searchValue}%`,
            },
          },
        ],
      },
      include: [{ model: FileDB, duplicating: false }],
    });

    return {
      stackList,
    };
  }

  static async getById(stackId: string) {
    const stack = await Stack.findByPk(stackId);

    if (!stack) {
      throwError({
        statusCode: 404,
        message: 'No stack found.',
      });
    }

    return stack;
  }

  static async create(dto: StackCreateDto) {
    const stack = await Stack.create({ ...dto });

    return stack;
  }

  static async updateById(dto: StackUpdateDto) {
    let stack = await SAStackService.getById(dto.stackId);

    await stack.update(dto);

    return await SAStackService.getById(dto.stackId);
  }

  static async deleteById(stackId: string) {
    const stack = await SAStackService.getById(stackId);

    await stack.destroy();

    return SwaggerUtils.messageOk();
  }
}
