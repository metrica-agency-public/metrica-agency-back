import FileDB from 'database/models/final/file-db.model';
import ProjectProperty from 'database/models/final/project-property.model';
import Project from 'database/models/final/project.model';
import Stack from 'database/models/final/stack.model';
import { ProjectCreateDto } from 'modules/dto/project-create.dto';
import { ProjectUpdateDto } from 'modules/dto/project-update.dto';
import { ParsedQs } from 'qs';
import { Op, literal, WhereOptions } from 'sequelize';
import SwaggerUtils from 'swagger/swagger-utils';
import { QueryShowType } from 'utils/constants';
import { throwError } from 'utils/http-exception';

export default class SAProjectsService {
  static async getAll(query: ParsedQs) {
    const projectsWhere: WhereOptions = {
      ...(query.show
        ? query.show === QueryShowType.ALL
          ? {}
          : query.show === QueryShowType.EXISTING
          ? { isExist: true }
          : { isExist: false }
        : {}),
      ...(query.searchValue
        ? {
            [Op.or]: [
              {
                title_ru: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
              {
                title_en: {
                  [Op.iLike]: `%${query.searchValue}%`,
                },
              },
            ],
          }
        : {}),
    };

    const projectList = await Project.findAll({
      where: projectsWhere,
      include: [
        {
          model: ProjectProperty,
          duplicating: false,
          separate: true,
          order: [['serialNumber', 'ASC']],
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
        },
        {
          model: Stack,
          through: {
            attributes: ['serialNumber'],
          },
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'logo',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'image',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'video',
          duplicating: false,
        },
      ],      
      order: [
        ['serialNumber', 'ASC'],
        [literal(`"stackList.ProjectStack.serialNumber"`), 'ASC'],
      ],
    });

    return {
      projectList,
    };
  }

  static async searchProject(searchValue: string) {
    const projectList = await Project.findAll({
      where: {
        title: {
          [Op.iLike]: `%${searchValue}%`,
        },
      },
    });
    return projectList;
  }

  static async getById(projectId: string) {
    const project = await Project.findByPk(projectId, {
      include: [
        {
          model: FileDB,
          as: 'logo',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'image',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'video',
          duplicating: false,
        },
        {
          model: ProjectProperty,
          duplicating: false,
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
        },
        {
          model: Stack,
          through: {
            attributes: ['serialNumber'],
          },
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
          duplicating: false,
        },
      ],
      order: [
        [literal(`"stackList.ProjectStack.serialNumber"`), 'ASC'],
      ],
    });

    if (!project) {
      throwError({
        statusCode: 404,
        message: 'No project found.',
      });
    }

    return project;
  }

  static async create(dto: ProjectCreateDto) {
    const project = await Project.create({
      ...dto,
    });

    return await SAProjectsService.getById(project.id);
  }

  static async updateById(dto: ProjectUpdateDto) {
    const project = await SAProjectsService.getById(dto.projectId);

    await project.update({
      ...dto,
    });

    return await SAProjectsService.getById(dto.projectId);
  }

  static async deleteById(id: string) {
    const project = await SAProjectsService.getById(id);

    await project.update({
      isExist: false,
    });

    return await SAProjectsService.getById(id);
  }

  static async deleteForeverById(id: string) {
    const project = await SAProjectsService.getById(id);

    await project.destroy();

    return SwaggerUtils.messageOk();
  }

  static async restoreById(id: string) {
    const project = await SAProjectsService.getById(id);

    await project.update({
      isExist: true,
    });

    return await SAProjectsService.getById(id);
  }
}
