import MainPageMetaInfo from 'database/models/final/main-page-meta-info.model';
import { Lang } from 'utils/constants';
import LangAttributes from 'utils/utils-lang';

export default class MetaInfoService {
  static async getAll(currentLang: Lang) {
    const metainfoList = await MainPageMetaInfo.findAll({
      order: [['serialNumber', 'ASC']],
    });

    return {
      metainfoList: metainfoList.map((el) => LangAttributes.langMetaInfo(el, currentLang)),
    };
  }
}
