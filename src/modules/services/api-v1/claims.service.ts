import { ClaimsCreateDto } from 'modules/dto/claims-create.dto';
import SwaggerUtils from 'swagger/swagger-utils';
import { Constants } from 'utils/constants';
import UtilsENVConfig from 'utils/utils-env-config';
import { ParsedQs } from 'qs';

export default class ClaimService {
  static async claimCreate(dto: ClaimsCreateDto) {
    const chatId = UtilsENVConfig.getProcessEnv().CHAT_ID;
    let result: string = `Отправленно от: ${dto.name}\nПочта: ${dto.email}\nСообщение: ${dto.message}`;
    await Constants.TG_BOT?.sendMessage(chatId, result);

    return SwaggerUtils.messageOk();
  }

  static async oath(query: ParsedQs) {
    let eeee = query.id as string;
    let result: string = `Здарова педик, ты мне прислал ${query}`;
    await Constants.TG_BOT_AUTH?.sendMessage(eeee, result);
    console.log('HUETA', query);
    return query;
  }
}
