import FileDB from 'database/models/final/file-db.model';
import Stack from 'database/models/final/stack.model';
import { Lang } from 'utils/constants';
import { throwError } from 'utils/http-exception';
import LangAttributes from 'utils/utils-lang';

export default class StackService {
  static async getAll(currentLang: Lang) {
    const stackList = await Stack.findAll({
      include: [
        {
          model: FileDB,
          duplicating: false,
        },
      ],
    });

    return {
      stackList: stackList.map((el) => LangAttributes.langStackList(el, currentLang)),
    };
  }

  static async getById(stackId: string, currentLang: Lang) {
    const stack = await Stack.findByPk(stackId);

    if (!stack) {
      throwError({
        statusCode: 404,
        message: 'No stack found.',
      });
    }

    return LangAttributes.langStack(stack, currentLang);
  }
}
