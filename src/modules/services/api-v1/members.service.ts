import Contact from 'database/models/final/contact.model';
import FileDB from 'database/models/final/file-db.model';
import Member from 'database/models/final/member.model';
import Stack from 'database/models/final/stack.model';
import { literal } from 'sequelize';
import { Lang } from 'utils/constants';
import { throwError } from 'utils/http-exception';
import LangAttributes from 'utils/utils-lang';

export default class MembersService {
  static async getAll(lang: Lang) {
    const memberList = await Member.findAll({
      where: {
        isExist: true,
      },
      order: [
        ['serialNumber', 'ASC'],
        ['contactList', 'serialNumber', 'ASC'],
      ],
      include: [
        {
          model: Contact,
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'fullImage',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'previewImage',
          duplicating: false,
        },
      ],
    });
    return {
      memberList: memberList.map((el) => LangAttributes.langMember(el, lang)),
    };
  }

  static async getById(id: string, lang: Lang) {
    let member = await Member.findByPk(id, {
      order: [
        ['contactList', 'serialNumber', 'ASC'],
        [literal(`"stackList.MemberStack.serialNumber"`), 'ASC'],
      ],
      include: [
        {
          model: FileDB,
          as: 'fullImage',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'previewImage',
          duplicating: false,
        },
        {
          model: Contact,
          duplicating: false,
        },
        {
          model: Stack,
          through: {
            attributes: ['serialNumber'],
          },
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
          duplicating: false,
        },
      ],
    });
    if (!member) {
      throwError({
        statusCode: 404,
        message: 'No member found.',
      });
    }
    return LangAttributes.langMember(member, lang);
  }
}
