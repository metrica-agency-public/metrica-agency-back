import FileDB from 'database/models/final/file-db.model';
import ProjectProperty from 'database/models/final/project-property.model';
import Project from 'database/models/final/project.model';
import Stack from 'database/models/final/stack.model';
import { literal } from 'sequelize';
import { Lang } from 'utils/constants';
import { throwError } from 'utils/http-exception';
import LangAttributes from 'utils/utils-lang';

export default class ProjectsService {
  static async getAll(lang: Lang) {
    const projectList = await Project.findAll({
      where: { isExist: true },
      order: [
        ['serialNumber', 'ASC'],
        ['propertyList', 'serialNumber', 'ASC'],
      ],
      include: [
        {
          model: ProjectProperty,
          duplicating: false,
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
        },
        {
          model: FileDB,
          as: 'logo',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'image',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'video',
          duplicating: false,
        },
      ],
    });

    return {
      projectList: projectList.map((el) => LangAttributes.langProject(el, lang)),
    };
  }

  static async getById(projectId: string, currentLang: Lang) {
    const project = await Project.findByPk(projectId, {
      order: [
        ['propertyList', 'serialNumber', 'ASC'],
        [literal(`"stackList.ProjectStack.serialNumber"`), 'ASC'],
      ],
      include: [
        {
          model: FileDB,
          as: 'logo',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'image',
          duplicating: false,
        },
        {
          model: FileDB,
          as: 'video',
          duplicating: false,
        },
        {
          model: ProjectProperty,
          duplicating: false,
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
        },
        {
          model: Stack,
          through: {
            attributes: ['serialNumber'],
          },
          include: [
            {
              model: FileDB,
              duplicating: false,
            },
          ],
          duplicating: false,
        },
      ],
    });

    if (!project) {
      throwError({
        statusCode: 404,
        message: 'No project found.',
      });
    }

    return LangAttributes.langProject(project, currentLang);
  }
}
