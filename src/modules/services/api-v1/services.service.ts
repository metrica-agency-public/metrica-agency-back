import FileDB from 'database/models/final/file-db.model';
import Service from 'database/models/final/service.model';
import { Lang } from 'utils/constants';
import { throwError } from 'utils/http-exception';
import LangAttributes from 'utils/utils-lang';

export default class ServicesService {
  static async getAll(currentLang: Lang) {
    const serviceList = await Service.findAll({
      order: [['serialNumber', 'ASC']],
      where: { isExist: true },
      include: [
        {
          model: FileDB,
          duplicating: false,
        },
      ],
    });

    return {
      serviceList: serviceList.map((el) => LangAttributes.langService(el, currentLang)),
    };
  }

  static async getById(id: string, currentLang: Lang) {
    let service = await Service.findByPk(id, {
      include: [
        {
          model: FileDB,
          duplicating: false,
        },
      ],
    });

    if (!service) {
      throwError({
        statusCode: 404,
        message: 'No service found.',
      });
    }

    return LangAttributes.langService(service, currentLang);
  }
}
