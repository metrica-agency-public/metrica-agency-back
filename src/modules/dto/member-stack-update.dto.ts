import { IsNotEmpty, IsNumber } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class MemberStackUpdateDto extends BaseDto {
  memberId!: string;
  stackId!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;
}
