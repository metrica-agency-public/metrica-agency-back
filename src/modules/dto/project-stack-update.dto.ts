import { IsNotEmpty, IsNumber } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class ProjectStackUpdateDto extends BaseDto {
  projectId!: string;
  stackId!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;
}
