import { IsEnum, IsNotEmpty, IsNumber, IsString, IsUUID } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';
import { MemberGroup } from 'utils/constants';

export class MemberCreateDto extends BaseDto {
  @IsString()
  @IsNotEmpty()
  firstName_ru!: string;

  @IsString()
  @IsNotEmpty()
  firstName_en!: string;

  @IsString()
  @IsNotEmpty()
  lastName_ru!: string;

  @IsString()
  @IsNotEmpty()
  lastName_en!: string;

  @IsString()
  @IsNotEmpty()
  about_ru!: string;

  @IsString()
  @IsNotEmpty()
  about_en!: string;

  @IsString()
  @IsNotEmpty()
  skills_ru!: string;

  @IsString()
  @IsNotEmpty()
  skills_en!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;

  @IsUUID(4)
  @IsNotEmpty()
  previewImageId!: string;

  @IsUUID(4)
  @IsNotEmpty()
  fullImageId!: string;

  @IsEnum(MemberGroup)
  @IsNotEmpty()
  group!: string;
}
