import { IsNotEmpty, IsNumber, IsOptional, IsString, IsUUID } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class ProjectCreateDto extends BaseDto {
  @IsString()
  @IsNotEmpty()
  title_ru!: string;

  @IsString()
  @IsNotEmpty()
  title_en!: string;

  @IsString()
  @IsNotEmpty()
  about_ru!: string;

  @IsString()
  @IsNotEmpty()
  about_en!: string;

  @IsString()
  @IsNotEmpty()
  color!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;

  @IsUUID(4)
  @IsNotEmpty()
  logoId!: string;

  @IsUUID(4)
  @IsNotEmpty()
  imageId!: string;

  @IsUUID(4)
  @IsNotEmpty()
  @IsOptional()
  videoId!: string;
}
