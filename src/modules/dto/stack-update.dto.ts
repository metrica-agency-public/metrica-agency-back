import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class StackUpdateDto extends BaseDto {
  stackId!: string;

  @IsString()
  @IsNotEmpty()
  title_ru!: string;

  @IsString()
  @IsNotEmpty()
  title_en!: string;

  @IsString()
  @IsNotEmpty()
  description_ru!: string;

  @IsString()
  @IsNotEmpty()
  description_en!: string;

  @IsUUID(4)
  @IsNotEmpty()
  imageId!: string;
}
