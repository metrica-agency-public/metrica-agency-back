import { IsNotEmpty, IsString } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class LoginDto extends BaseDto {
  @IsString()
  @IsNotEmpty()
  login!: string;

  @IsString()
  @IsNotEmpty()
  password!: string;
}
