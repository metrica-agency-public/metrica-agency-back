import { IsNotEmpty, IsNumber, IsUUID } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class MemberStackCreateDto extends BaseDto {
  memberId!: string;

  @IsUUID(4)
  @IsNotEmpty()
  stackId!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;
}
