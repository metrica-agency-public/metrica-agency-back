import { IsEnum, IsNotEmpty, IsNumber, IsString, IsUUID } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';
import { ProjectPropertyImagePosition } from 'utils/constants';

export class ProjectPropertyUpdateDto extends BaseDto {
  projectId!: string; //пока оставлю, мб пригодится
  propertyId!: string;

  @IsString()
  @IsNotEmpty()
  title_ru!: string;

  @IsString()
  @IsNotEmpty()
  title_en!: string;

  @IsString()
  @IsNotEmpty()
  description_ru!: string;

  @IsString()
  @IsNotEmpty()
  description_en!: string;

  @IsEnum(ProjectPropertyImagePosition)
  @IsString()
  @IsNotEmpty()
  imagePosition!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;

  @IsUUID(4)
  @IsNotEmpty()
  imageId!: string;
}
