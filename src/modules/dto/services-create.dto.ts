import { IsNotEmpty, IsNumber, IsString, IsUUID } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class ServicesCreateDto extends BaseDto {
  @IsString()
  @IsNotEmpty()
  public title_ru!: string;

  @IsString()
  @IsNotEmpty()
  public title_en!: string;

  @IsString()
  @IsNotEmpty()
  public about_ru!: string;

  @IsString()
  @IsNotEmpty()
  public about_en!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;

  @IsUUID(4)
  @IsNotEmpty()
  imageId!: string;
}
