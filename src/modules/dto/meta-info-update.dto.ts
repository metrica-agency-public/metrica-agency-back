import { IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';

export class MetaInfoUpdateDto extends BaseDto {
  id!: string;

  @IsString()
  @IsNotEmpty()
  title_ru!: string;

  @IsString()
  @IsNotEmpty()
  title_en!: string;

  @IsString()
  @IsNotEmpty()
  description_ru!: string;

  @IsString()
  @IsNotEmpty()
  description_en!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;
}
