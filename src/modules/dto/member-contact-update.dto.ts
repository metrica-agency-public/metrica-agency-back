import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';
import { ContactTypes } from 'utils/constants';

export class MemberContactUpdateDto extends BaseDto {
  contactId!: string;
  memberId!: string;

  @IsEnum(ContactTypes)
  @IsNotEmpty()
  type!: string;

  @IsString()
  @IsNotEmpty()
  value!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;
}
