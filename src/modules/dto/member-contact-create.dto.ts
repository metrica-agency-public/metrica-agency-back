import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { BaseDto } from 'modules/base/base.dto';
import { ContactTypes } from 'utils/constants';

export class MemberContactCreateDto extends BaseDto {
  memberId!: string;

  @IsEnum(ContactTypes)
  @IsNotEmpty()
  type!: string;

  @IsString()
  @IsNotEmpty()
  value!: string;

  @IsNumber()
  @IsNotEmpty()
  serialNumber!: number;
}
