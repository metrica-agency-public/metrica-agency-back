import { Request } from 'express';
import { Lang } from 'utils/constants';

export default interface BaseRequest extends Request {
  value: string;
  userId: string;

  currentLang: Lang;
  accessToken: string;
}
