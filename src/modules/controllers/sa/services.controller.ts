import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { tokenRequire } from 'middlewares/token-require';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { ServicesCreateDto } from 'modules/dto/services-create.dto';
import { ServicesUpdateDto } from 'modules/dto/services-update.dto';
import SAServicesService from 'modules/services/sa/sa-services.service';
import SAServicesSwaggerModels from 'swagger/swagger-models/sa/services';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/sa/services')
class SAServicesController {
  @GET('/', {
    summary: 'Получение списка всех услуг',
    handlers: [tokenRequire],
    query: {
      searchValue: '',
      show: '',
    },
    responses: [SwaggerUtils.body200(SAServicesSwaggerModels.responseServiceList)],
  })
  async getAll(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAServicesService.getAll(req.query);
    res.json(result);
  }

  @GET('/:id', {
    summary: 'Получения услуги по id',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAServicesSwaggerModels.responseService)],
  })
  async getServiceById(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAServicesService.getById(req.params.id);
    res.json(result);
  }

  @POST('/', {
    summary: 'Создание услуги',
    handlers: [tokenRequire, dtoValidator(ServicesCreateDto)],
    body: SAServicesSwaggerModels.requestServiceCreate,
    responses: [SwaggerUtils.body200(SAServicesSwaggerModels.responseService)],
  })
  async createService(req: BaseRequest, res: Response, next: NextFunction) {
    let dto: ServicesCreateDto = req.body;
    const result = await SAServicesService.create(dto);
    res.json(result);
  }

  @PATCH('/:id', {
    summary: 'Изменение параметров услуги',
    handlers: [tokenRequire, dtoValidator(ServicesUpdateDto)],
    body: SAServicesSwaggerModels.requestServiceCreate,
    responses: [SwaggerUtils.body200(SAServicesSwaggerModels.responseService)],
  })
  async updateService(req: BaseRequest, res: Response, next: NextFunction) {
    let dto: ServicesUpdateDto = req.body;
    dto.serviceId = req.params.id;
    const result = await SAServicesService.updateById(dto);
    res.json(result);
  }

  @PATCH('/:id/restore', {
    summary: 'Востановление услуги',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAServicesSwaggerModels.responseService)],
  })
  async restoreService(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAServicesService.restoreById(req.params.id);
    res.json(result);
  }

  @DELETE('/:id/forever', {
    summary: 'Удаление услуги (НАВСЕГДА)',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAServicesSwaggerModels.responseService)],
  })
  async deleteServiceForever(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAServicesService.deleteForeverById(req.params.id);
    res.json(result);
  }

  @DELETE('/:id', {
    summary: 'Удаление услуги',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAServicesSwaggerModels.responseService)],
  })
  async deleteService(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAServicesService.deleteById(req.params.id);
    res.json(result);
  }
}

export default new SAServicesController();
