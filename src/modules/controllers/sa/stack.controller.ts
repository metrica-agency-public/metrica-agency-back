import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { tokenRequire } from 'middlewares/token-require';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { StackCreateDto } from 'modules/dto/stack-create.dto';
import { StackUpdateDto } from 'modules/dto/stack-update.dto';
import SAStackService from 'modules/services/sa/sa-stack.service';
import SAStackSwaggerModels from 'swagger/swagger-models/sa/stack';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/sa/stack')
class SAStackController {
  @GET('/', {
    summary: 'Получение списка стека',
    handlers: [tokenRequire],
    query: {
      searchValue: '',
      show: '',
    },
    responses: [SwaggerUtils.body200(SAStackSwaggerModels.responseStackList)],
  })
  async getAllStack(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAStackService.getAll(req.query.searchValue as string);
    res.json(result);
  }

  @GET('/:id', {
    summary: 'Получение стека по id',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAStackSwaggerModels.requestStack)],
  })
  async getStackById(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAStackService.getById(req.params.id);
    res.json(result);
  }

  @POST('/', {
    summary: 'Добавление стека',
    handlers: [tokenRequire, dtoValidator(StackCreateDto)],
    body: SAStackSwaggerModels.responseStack,
    responses: [SwaggerUtils.body200(SAStackSwaggerModels.requestStack)],
  })
  async addStack(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: StackCreateDto = req.body;
    const result = await SAStackService.create(dto);
    res.json(result);
  }

  @PATCH('/:id', {
    summary: 'Обновление стека',
    handlers: [tokenRequire, dtoValidator(StackUpdateDto)],
    body: SAStackSwaggerModels.responseStack,
    responses: [SwaggerUtils.body200(SAStackSwaggerModels.requestStack)],
  })
  async updateStack(req: BaseRequest, res: Response, next: NextFunction) {
    let dto: StackUpdateDto = req.body;
    dto.stackId = req.params.id;
    const result = await SAStackService.updateById(dto);
    res.json(result);
  }

  @DELETE('/:id', {
    summary: 'Удаление стека',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.message200()],
  })
  async deleteStack(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAStackService.deleteById(req.params.id);
    res.json(result);
  }
}

export default new SAStackController();
