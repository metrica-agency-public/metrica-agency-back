import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { tokenRequire } from 'middlewares/token-require';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { MetaInfoCreateDto } from 'modules/dto/meta-info-create.dto';
import { MetaInfoUpdateDto } from 'modules/dto/meta-info-update.dto';
import SAMetaInfoService from 'modules/services/sa/sa-meta-info.service';
import SAMetaSwaggerModels from 'swagger/swagger-models/sa/meta-info';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/sa/meta-info')
class SAMetaInfoController {
  @GET('/', {
    summary: 'Получение мета информации',
    handlers: [tokenRequire],
    query: {
      searchValue: '',
    },
    responses: [SwaggerUtils.body200(SAMetaSwaggerModels.responseMetaInfoList)],
  })
  async getMetaInfo(req: BaseRequest, res: Response, next: NextFunction) {
    const searchValue = req.query.searchValue as string;
    const result = await SAMetaInfoService.getAll(searchValue);
    res.json(result);
  }

  @POST('/', {
    summary: 'Добавление мета информации',
    handlers: [tokenRequire, dtoValidator(MetaInfoCreateDto)],
    body: SAMetaSwaggerModels.requestMetaInfo,
    responses: [SwaggerUtils.body200(SAMetaSwaggerModels.responseMetaInfo)],
  })
  async addMetaInfo(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MetaInfoCreateDto = req.body;
    const result = await SAMetaInfoService.create(dto);
    res.json(result);
  }

  @PATCH('/:id', {
    summary: 'Обновление мета информации',
    handlers: [tokenRequire, dtoValidator(MetaInfoUpdateDto)],
    body: SAMetaSwaggerModels.requestMetaInfo,
    responses: [SwaggerUtils.body200(SAMetaSwaggerModels.responseMetaInfo)],
  })
  async updateMetaInfo(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MetaInfoUpdateDto = req.body;
    dto.id = req.params.id;
    const result = await SAMetaInfoService.updateById(dto);
    res.json(result);
  }

  @DELETE('/:id', {
    summary: 'Удаление мета информации',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.message200()],
  })
  async deleteMetaInfo(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMetaInfoService.deleteById(req.params.id);
    res.json(result);
  }
}

export default new SAMetaInfoController();
