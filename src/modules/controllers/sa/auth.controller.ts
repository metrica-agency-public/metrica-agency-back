import { ApiController, GET, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { tokenRequire } from 'middlewares/token-require';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { LoginDto } from 'modules/dto/login.dto';
import AuthService from 'modules/services/auth.service';

@ApiController('/api/v1/sa/auth') //мейби не аутх, а личный кабинет?
class SAAuthController {
  @POST('/login', {
    summary: 'Авторизация по login + password',
    handlers: [dtoValidator(LoginDto)],
  })
  async login(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: LoginDto = req.body;
    const result = await AuthService.login(dto);
    res.json(result);
  }

  @GET('/check', {
    summary: 'Проверка токена',
    handlers: [tokenRequire],
  })
  async checkToken(req: BaseRequest, res: Response, next: NextFunction) {
    res.json({
      message: 'Ok',
    });
  }

  @POST('/logout', {
    summary: 'Логаут админа. Выход из аккаунта',
    handlers: [tokenRequire],
  })
  async logout(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await AuthService.logout(req.accessToken);
    res.json(result);
  }
}

export default new SAAuthController();
