import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { tokenRequire } from 'middlewares/token-require';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { MemberContactCreateDto } from 'modules/dto/member-contact-create.dto';
import { MemberContactUpdateDto } from 'modules/dto/member-contact-update.dto';
import { MemberStackCreateDto } from 'modules/dto/member-stack-create.dto';
import { MemberStackUpdateDto } from 'modules/dto/member-stack-update.dto';
import { MemberCreateDto } from 'modules/dto/members-create.dto';
import { MemberUpdateDto } from 'modules/dto/members-update.dto';
import SAMembersContactService from 'modules/services/sa/sa-member-contact.service';
import SAMembersStackService from 'modules/services/sa/sa-member-stack.service';
import SAMembersService from 'modules/services/sa/sa-member.service';
import SAMembersSwaggerModels from 'swagger/swagger-models/sa/members';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/sa/members')
class SAMemberController {
  @GET('/', {
    summary: 'Получение списка всех членов команды',
    handlers: [tokenRequire],
    query: {
      searchValue: '',
      show: '',
    },
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberListGetAll)],
  })
  async getAll(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMembersService.getAll(req.query);
    res.json(result);
  }

  @GET('/:id', {
    summary: 'Получения члена команды по id',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async getMemberById(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMembersService.getById(req.params.id);
    res.json(result);
  }

  @POST('/', {
    summary: 'Создание члена команды',
    handlers: [tokenRequire, dtoValidator(MemberCreateDto)],
    body: SAMembersSwaggerModels.requestCreateUpdateMember,
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async createMember(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MemberCreateDto = req.body;
    const result = await SAMembersService.create(dto);
    res.json(result);
  }

  @PATCH('/:id', {
    summary: 'Изменение параметров члена команды',
    handlers: [tokenRequire, dtoValidator(MemberUpdateDto)],
    body: SAMembersSwaggerModels.requestCreateUpdateMember,
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async updateMember(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MemberUpdateDto = req.body;
    dto.memberId = req.params.id;
    const result = await SAMembersService.updateById(dto);
    res.json(result);
  }

  @DELETE('/:id/forever', {
    summary: 'Удаление члена команды (НАВСЕГДА)',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.message200()],
  })
  async deleteMemberForever(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMembersService.deleteForeverById(req.params.id);
    res.json(result);
  }

  @PATCH('/:id/restore', {
    summary: 'Восстановление члена команды',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.message200()],
  })
  async restoreMember(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMembersService.restoreById(req.params.id);
    res.json(result);
  }

  @DELETE('/:id', {
    summary: 'Удаление члена команды',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.message200()],
  })
  async deleteMember(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMembersService.deleteById(req.params.id);
    res.json(result);
  }

  //Стек
  @POST('/:id/stack', {
    summary: 'Добавление стека для члена команды',
    handlers: [tokenRequire, dtoValidator(MemberStackCreateDto)],
    body: SAMembersSwaggerModels.requestCreateUpdateMemberStack,
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async addStackItem(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MemberStackCreateDto = req.body;
    dto.memberId = req.params.id;
    const result = await SAMembersStackService.add(dto);
    res.json(result);
  }

  @PATCH('/:memberId/stack/:stackId', {
    summary: 'Изменение параметров КОНКРЕТНОГО стека у участника',
    handlers: [tokenRequire, dtoValidator(MemberStackUpdateDto)],
    body: SAMembersSwaggerModels.requestCreateUpdateMemberStack,
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async updateStackItem(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MemberStackUpdateDto = req.body;
    dto.memberId = req.params.memberId;
    dto.stackId = req.params.stackId;
    const result = await SAMembersStackService.updateById(dto);
    res.json(result);
  }

  @DELETE('/:memberId/stack/:stackId', {
    summary: 'Удаление стека у участника',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async deleteStackItem(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMembersStackService.deleteById(req.params.memberId, req.params.stackId);
    res.json(result);
  }

  //Контакты
  @POST('/:id/contacts', {
    summary: 'Создание контакта, для члена команды',
    handlers: [tokenRequire, dtoValidator(MemberContactCreateDto)],
    body: SAMembersSwaggerModels.requestCreateMemberContact,
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async createContact(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MemberContactCreateDto = req.body;
    dto.memberId = req.params.id;
    const result = await SAMembersContactService.create(dto);
    res.json(result);
  }

  @PATCH('/:memberId/contacts/:contactId', {
    summary: 'Изменение параметров контакта одного из членов команды',
    handlers: [tokenRequire, dtoValidator(MemberContactUpdateDto)],
    body: SAMembersSwaggerModels.requestUpdateMemberContact,
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async updateContact(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: MemberContactUpdateDto = req.body;
    dto.contactId = req.params.contactId;
    dto.memberId = req.params.memberId;
    const result = await SAMembersContactService.updateById(dto);
    res.json(result);
  }

  @DELETE('/:memberId/contacts/:contactId', {
    summary: 'Удаление контактов',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAMembersSwaggerModels.responseMemberById)],
  })
  async deleteContact(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAMembersContactService.deleteById(req.params.memberId, req.params.contactId);
    res.json(result);
  }
}

export default new SAMemberController();
