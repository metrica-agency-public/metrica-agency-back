import { ApiController, DELETE, GET, PATCH, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { tokenRequire } from 'middlewares/token-require';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { ProjectCreateDto } from 'modules/dto/project-create.dto';
import { ProjectPropertyCreateDto } from 'modules/dto/project-property-create.dto';
import { ProjectPropertyUpdateDto } from 'modules/dto/project-property-update.dto';
import { ProjectStackCreateDto } from 'modules/dto/project-stack-create.dto';
import { ProjectStackUpdateDto } from 'modules/dto/project-stack-update.dto';
import { ProjectUpdateDto } from 'modules/dto/project-update.dto';
import SAProjectPropertyService from 'modules/services/sa/sa-project-property.service';
import SAProjectStackService from 'modules/services/sa/sa-project-stack.service';
import SAProjectsService from 'modules/services/sa/sa-projects.service';
import SAProjectsSwaggerModels from 'swagger/swagger-models/sa/projects';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/sa/projects')
class SAProjectsController {
  @GET('/', {
    summary: 'Получение списка всех проектов',
    handlers: [tokenRequire],
    query: {
      searchValue: '',
      show: '',
    },
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProjectList)],
  })
  async getAllProjects(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAProjectsService.getAll(req.query);
    res.json(result);
  }

  @GET('/:id', {
    summary: 'Получение проекта по id',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async getProjectById(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAProjectsService.getById(req.params.id);
    res.json(result);
  }

  @GET('/search', {
    summary: 'Получение стека через поиск',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.requestProjectsSearch)],
  })
  async searchStack(req: BaseRequest, res: Response, next: NextFunction) {
    const sarchValue = req.query.searchValue as string;
    const result = await SAProjectsService.searchProject(sarchValue);
    res.json(result);
  }

  @POST('/', {
    summary: 'Добавление проекта',
    handlers: [tokenRequire, dtoValidator(ProjectCreateDto)],
    body: SAProjectsSwaggerModels.requestProjectCreate,
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async addProject(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: ProjectCreateDto = req.body;
    const result = await SAProjectsService.create(dto);
    res.json(result);
  }

  @PATCH('/:id', {
    summary: 'Обновление проекта',
    handlers: [tokenRequire, dtoValidator(ProjectUpdateDto)],
    body: SAProjectsSwaggerModels.requestProjectCreate,
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async updateProject(req: BaseRequest, res: Response, next: NextFunction) {
    let dto: ProjectUpdateDto = req.body;
    dto.projectId = req.params.id;
    const result = await SAProjectsService.updateById(dto);
    res.json(result);
  }

  @PATCH('/:id/restore', {
    summary: 'Восстановление проекта',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async restoreProject(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAProjectsService.restoreById(req.params.id);
    res.json(result);
  }

  @DELETE('/:id/forever', {
    summary: 'Удаление проекта (навсегда)',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async deleteProjectForever(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAProjectsService.deleteForeverById(req.params.id);
    res.json(result);
  }

  @DELETE('/:id', {
    summary: 'Удаление проекта',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async deleteProject(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAProjectsService.deleteById(req.params.id);
    res.json(result);
  }

  //Стек
  @POST('/:id/stack', {
    summary: 'Добавление стека к проекту',
    handlers: [tokenRequire, dtoValidator(ProjectStackCreateDto)],
    body: SAProjectsSwaggerModels.requestProjectStackCreate,
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async addStackItem(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: ProjectStackCreateDto = req.body;
    dto.projectId = req.params.id;
    const result = await SAProjectStackService.add(dto);
    res.json(result);
  }

  @PATCH('/:projectId/stack/:stackId', {
    summary: 'Изменение параметров КОНКРЕТНОГО стека у проекта',
    handlers: [tokenRequire, dtoValidator(ProjectStackUpdateDto)],
    body: SAProjectsSwaggerModels.requestProjectStackUpdate,
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async updateStackItem(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: ProjectStackUpdateDto = req.body;
    dto.projectId = req.params.projectId;
    dto.stackId = req.params.stackId;
    const result = await SAProjectStackService.updateById(dto);
    res.json(result);
  }

  @DELETE('/:projectId/stack/:stackId', {
    summary: 'Удаление стека у проекта',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async deleteStackItem(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAProjectStackService.deleteById(req.params.projectId, req.params.stackId);
    res.json(result);
  }

  //Свойства проекта
  @POST('/:id/project-property', {
    summary: 'Добавление свойств проекта',
    handlers: [tokenRequire, dtoValidator(ProjectPropertyCreateDto)],
    body: SAProjectsSwaggerModels.requestProjectPropertyCreate,
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async addProjectProperty(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: ProjectPropertyCreateDto = req.body;
    dto.projectId = req.params.id;
    const result = await SAProjectPropertyService.create(dto);
    res.json(result);
  }

  @PATCH('/:projectId/project-property/:id', {
    summary: 'Обновление свойства проекта',
    handlers: [tokenRequire, dtoValidator(ProjectPropertyUpdateDto)],
    body: SAProjectsSwaggerModels.requestProjectPropertyCreate,
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async updateProjectProperty(req: BaseRequest, res: Response, next: NextFunction) {
    let dto: ProjectPropertyUpdateDto = req.body;
    dto.projectId = req.params.projectId;
    dto.propertyId = req.params.id;
    const result = await SAProjectPropertyService.updateById(dto);
    res.json(result);
  }

  @DELETE('/:projectId/project-property/:id', {
    summary: 'Удаление свойства проекта',
    handlers: [tokenRequire],
    responses: [SwaggerUtils.body200(SAProjectsSwaggerModels.responseResProject)],
  })
  async deleteProjectProperty(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await SAProjectPropertyService.deleteById(req.params.projectId, req.params.id);
    res.json(result);
  }
}

export default new SAProjectsController();
