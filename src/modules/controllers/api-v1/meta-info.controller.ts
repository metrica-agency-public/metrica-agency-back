import { ApiController, GET } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { addLang } from 'middlewares/add-lang';
import BaseRequest from 'modules/base/base.request';
import MetaInfoService from 'modules/services/api-v1/meta-info.service';
import ApiV1SwaggerModels from 'swagger/swagger-models/api-v1/api-v1-models';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/meta-info')
class MetaInfoController {
  @GET('/', {
    summary: 'Получение списка с метаинформацией для главной страницы',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resMetaInfoList)],
  })
  async getMetaInfo(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await MetaInfoService.getAll(req.currentLang);
    res.json(result);
  }
}

export default new MetaInfoController();
