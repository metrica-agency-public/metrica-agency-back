import { ApiController, GET } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { addLang } from 'middlewares/add-lang';
import BaseRequest from 'modules/base/base.request';
import MembersService from 'modules/services/api-v1/members.service';
import ApiV1SwaggerModels from 'swagger/swagger-models/api-v1/api-v1-models';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/members')
class MembersController {
  @GET('/', {
    summary: 'Получение списка всех членов команды',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resMemberList)],
  })
  async getAll(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await MembersService.getAll(req.currentLang);
    res.json(result);
  }

  @GET('/:id', {
    summary: 'Получения участника по id',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resMember)],
  })
  async getMemberById(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await MembersService.getById(req.params.id, req.currentLang);
    res.json(result);
  }
}

export default new MembersController();
