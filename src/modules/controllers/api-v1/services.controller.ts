import { ApiController, GET } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { addLang } from 'middlewares/add-lang';
import BaseRequest from 'modules/base/base.request';
import ServicesService from 'modules/services/api-v1/services.service';
import ApiV1SwaggerModels from 'swagger/swagger-models/api-v1/api-v1-models';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/services')
class ServicesController {
  @GET('/', {
    summary: 'Получение списка всех людей',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resServiceList)],
  })
  async getServices(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await ServicesService.getAll(req.currentLang);
    res.json(result);
  }
}

export default new ServicesController();
