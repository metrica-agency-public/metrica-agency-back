import { ApiController, GET } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { addLang } from 'middlewares/add-lang';
import BaseRequest from 'modules/base/base.request';
import StackService from 'modules/services/api-v1/stack.service';
import ApiV1SwaggerModels from 'swagger/swagger-models/api-v1/api-v1-models';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/stack')
class StackController {
  @GET('/', {
    summary: 'Получение списка стека',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resStackList)],
  })
  async getAllStack(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await StackService.getAll(req.currentLang);
    res.json(result);
  }

  @GET('/:id', {
    summary: 'Получение стека по id',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resStack)],
  })
  async getStackById(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await StackService.getById(req.params.id, req.currentLang);
    res.json(result);
  }
}

export default new StackController();
