import { ApiController, GET, POST } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { addLang } from 'middlewares/add-lang';
import { dtoValidator } from 'middlewares/validate';
import BaseRequest from 'modules/base/base.request';
import { ClaimsCreateDto } from 'modules/dto/claims-create.dto';
import ClaimService from 'modules/services/api-v1/claims.service';
import ApiV1SwaggerModels from 'swagger/swagger-models/api-v1/api-v1-models';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/claims')
class StackController {
  @POST('/', {
    summary: 'Отправить заявку',
    handlers: [addLang, dtoValidator(ClaimsCreateDto)],
    body: ApiV1SwaggerModels.reqClaimCreate,
    responses: [SwaggerUtils.message200()],
  })
  async getAllStack(req: BaseRequest, res: Response, next: NextFunction) {
    const dto: ClaimsCreateDto = req.body;
    const result = await ClaimService.claimCreate(dto);
    res.json(result);
  }

  @GET('/oauth', {
    summary: 'Отправить заявку',
    handlers: [],
    responses: [SwaggerUtils.message200()],
  })
  async oauth(req: BaseRequest, res: Response, next: NextFunction) {
    
    const result = await ClaimService.oath(req.query);
    res.json(result);
  }
}

export default new StackController();
