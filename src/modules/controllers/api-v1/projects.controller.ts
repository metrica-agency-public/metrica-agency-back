import { ApiController, GET } from 'core/api-decorators';
import { NextFunction, Response } from 'express';
import { addLang } from 'middlewares/add-lang';
import BaseRequest from 'modules/base/base.request';
import ProjectsService from 'modules/services/api-v1/projects.service';
import ApiV1SwaggerModels from 'swagger/swagger-models/api-v1/api-v1-models';
import SwaggerUtils from 'swagger/swagger-utils';

@ApiController('/api/v1/projects')
class ProjectsController {
  @GET('/', {
    summary: 'Получение списка всех проектов',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resProjectList)],
  })
  async getAllProjects(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await ProjectsService.getAll(req.currentLang);
    res.json(result);
  }

  @GET('/:id', {
    summary: 'Получение проекта по id',
    handlers: [addLang],
    responses: [SwaggerUtils.body200(ApiV1SwaggerModels.resProject)],
  })
  async getProjectById(req: BaseRequest, res: Response, next: NextFunction) {
    const result = await ProjectsService.getById(req.params.id, req.currentLang);
    res.json(result);
  }
}

export default new ProjectsController();
