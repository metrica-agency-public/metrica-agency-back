import { ContactTypes, FileTypes } from 'utils/constants';
import SAFileModels from '../sa/file-db';

export default class ApiV1SwaggerModels {
  static reqClaimCreate = {
    email: 'email@mail.ru',
    message: 'Сообщение',
  };

  static resFile = {
    id: 'UUID',
    extension: 'jpg,png...',
    size: 1234,
    originalName: 'картинка1234.jpg',
    type: Object.values(FileTypes).join('/'),
    url: 'https://....',
  };

  static resStack = {
    id: 'UUID',
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
  };

  static resStackList = {
    stackList: [ApiV1SwaggerModels.resStack],
    image: SAFileModels.resFileDB,
  };

  static resMemberStack = {
    ...ApiV1SwaggerModels.resStack,
    serialNumber: 1234,
  };

  static resMemberContact = {
    id: 'UUID',
    type: Object.values(ContactTypes).join('/'),
    value: '@zibellon',
    serialNumber: 1234,
  };

  static resMember = {
    id: 'UUID',
    firstName_ru: 'Имя',
    lastName_ru: 'Фамилия',

    firstName_en: 'Name',
    lastName_en: 'Surname',

    about_ru: 'Описание',
    about_en: 'About',

    skills_ru: 'Навыки',
    skills_en: 'Skills',
    serialNumber: 1234,
    contactList: [ApiV1SwaggerModels.resMemberContact],
    stackList: [ApiV1SwaggerModels.resMemberStack],
    fullImage: ApiV1SwaggerModels.resFile,
    previewImage: ApiV1SwaggerModels.resFile,
  };

  static resMemberList = {
    memberList: [ApiV1SwaggerModels.resMember],
  };

  static resProjectStack = {
    ...ApiV1SwaggerModels.resStack,
    serialNumber: 1234,
  };

  static responseProjectProperty = {
    id: 'UUID',
    image: SAFileModels.resFileDB,
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
    serialNumber: 1,
    imagePosition: 'ENUM.Позиция',
  };

  static resProject = {
    id: 'UUID',
    title_ru: 'Заголовок',
    title_en: 'Title',
    about_ru: 'Описание',
    about_en: 'About',
    color: 'HEX...',
    serialNumber: 1234,
    propertyList: [ApiV1SwaggerModels.responseProjectProperty],
    stackList: [ApiV1SwaggerModels.resProjectStack],
    logo: ApiV1SwaggerModels.resFile,
    image: ApiV1SwaggerModels.resFile,
    video: ApiV1SwaggerModels.resFile,
  };

  static resProjectList = {
    projectList: [ApiV1SwaggerModels.resProject],
  };

  static resMetaInfo = {
    id: 'UUID',
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
    serialNumber: 1234,
  };

  static resMetaInfoList = {
    metaInfoList: [ApiV1SwaggerModels.resMetaInfo],
  };

  static resService = {
    id: 'UUID',
    title_ru: 'Заголовок',
    title_en: 'Title',
    about_ru: 'Описание',
    about_en: 'About',
    serialNumber: 1234,
    image: ApiV1SwaggerModels.resFile,
  };

  static resServiceList = {
    serviceList: [ApiV1SwaggerModels.resService],
  };
}
