import { QueryShowType } from 'utils/constants';

export default class SAMetaSwaggerModels {
  //-----RESPONSE-----

  static responseMetaInfo = {
    id: 'UUID',
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
  };

  static responseMetaInfoList = {
    metaInfoList: [SAMetaSwaggerModels.responseMetaInfo],
  };

  //-----REQUEST-----

  static requestMetaInfo = {
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
  };

  static requestMetaInfoSearch = {
    searchValue: 'Манул',
    show: Object.values(QueryShowType).join('/'),
  };
}
