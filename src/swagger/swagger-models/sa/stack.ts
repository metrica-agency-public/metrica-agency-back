import { QueryShowType } from 'utils/constants';
import SAFileModels from './file-db';

export default class SAStackSwaggerModels {
  //-----RESPONSE-----

  static responseStack = {
    id: 'UUID',
    image: SAFileModels.resFileDB,
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
  };

  //На будующее
  static responseStackId(id: string) {
    return {
      id: id,
      image: SAFileModels.resFileDB,
      title_ru: 'Заголовок',
      title_en: 'Title',
      description_ru: 'Описание',
      description_en: 'Description',
    };
  }

  static responseStackList = {
    stackList: [SAStackSwaggerModels.responseStack],
  };

  //-----REQUEST-----

  static requestStack = {
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
    imageId: 'UUID',
  };

  // static requestStackId(id: string) {
  //     return {
  //         title: "Заголовок",
  //         description: "Описание",
  //         imageId: id,
  //     }
  // }

  static requestStackSearch = {
    //query
    searchValue: 'Манул',
    show: Object.values(QueryShowType).join('/'),
  };
}
