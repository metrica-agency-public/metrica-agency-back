import { ContactTypes } from 'utils/constants';
import SAFileModels from './file-db';

export default class SAMembersSwaggerModels {
  //Все UUID, которые приходят не в body я удалил
  static contact = {
    id: 'UUID',
    type: Object.values(ContactTypes).join('/'),
    value: '88005553535/@alcoholic',
    serialNumber: 1234,
  };

  static stack = {
    fileDb: SAFileModels.resFileDB,
  };

  static responseMemberById = {
    id: 'UUID',
    firstName_ru: 'Имя',
    lastName_ru: 'Фамилия',

    firstName_en: 'Name',
    lastName_en: 'Surname',

    about_ru: 'Описание',
    about_en: 'About',

    skills_ru: 'Навыки',
    skills_en: 'Skills',
    serialNumber: 1234,
    isExist: true,
    contact: [SAMembersSwaggerModels.contact],
    stack: [SAMembersSwaggerModels.stack],
    fullImage: SAFileModels.resFileDB,
    previewImage: SAFileModels.resFileDB,
  };

  static responseMemberListGetAll = {
    memberList: [SAMembersSwaggerModels.responseMemberById],
  };

  static responseMember = {
    id: 'UUID',
    firstName_ru: 'Имя',
    lastName_ru: 'Фамилия',

    firstName_en: 'Name',
    lastName_en: 'Surname',

    about_ru: 'Описание',
    about_en: 'About',

    skills_ru: 'Навыки',
    skills_en: 'Skills',

    serialNumber: 1234,
    isExist: true,
  };

  static responseMemberListSearch = {
    memberList: [SAMembersSwaggerModels.responseMember],
  };

  static requestCreateUpdateMember = {
    firstName_ru: 'Имя',
    lastName_ru: 'Фамилия',

    firstName_en: 'Name',
    lastName_en: 'Surname',

    about_ru: 'Описание',
    about_en: 'About',

    skills_ru: 'Навыки',
    skills_en: 'Skills',
    serialNumber: 1234,
    previewImageId: 'UUID',
    fullImageId: 'UUID',
  };

  static requestCreateUpdateMemberStack = {
    serialNumber: 1234,
  };

  static requestCreateMemberContact = {
    type: Object.values(ContactTypes).join('/'),
    value: '88005553535/@alcoholic',
    serialNumber: 1234,
  };

  static requestUpdateMemberContact = {
    type: Object.values(ContactTypes).join('/'),
    value: '88005553535/@alcoholic',
    serialNumber: 1234,
  };
}
