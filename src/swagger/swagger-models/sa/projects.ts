import { QueryShowType } from 'utils/constants';
import SAFileModels from './file-db';
import SAStackSwaggerModels from './stack';

export default class SAProjectsSwaggerModels {
  static responseProjectProperty = {
    id: 'UUID',
    image: SAFileModels.resFileDB,
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
    serialNumber: 1,
    imagePosition: 'ENUM.Позиция',
  };
  //На будующее
  // static responseProjectPropertyId(id: string) {
  //     return {
  //         id: id,
  //         title: "Заголовок",
  //         description: "Описание",
  //         serialNumber: 1,
  //         imagePosition: "ENUM.Позиция",
  //         image: FileModels.uploadedFile,
  //     }
  // }

  static responseResProject = {
    id: 'UUID',
    logo: SAFileModels.resFileDB,
    image: SAFileModels.resFileDB,
    video: SAFileModels.resFileDB,
    title_ru: 'Заголовок',
    title_en: 'Title',
    about_ru: 'Описание',
    about_en: 'About',
    color: 'Цвет',
    serialNumber: 1,
    isExist: true,
    propertyList: [SAProjectsSwaggerModels.responseProjectProperty],
    stackList: [SAStackSwaggerModels.responseStackList],
  };

  static responseResProjectList = {
    projectList: [SAProjectsSwaggerModels.responseResProject],
  };

  //-----REQUEST-----

  static requestProjectCreate = {
    title_ru: 'Заголовок',
    title_en: 'Title',
    about_ru: 'Описание',
    about_en: 'About',
    logoId: 'UUID',
    imageId: 'UUID',
    videoId: 'UUID',
    color: 'HEX', // ЦВЕТ - в формате HEX
    serialNumber: 1,
  };
  //На будующее
  // static requestProjectCreateId(logoId: string, imageId: string, videoId: string) {
  //     return {
  //         title: "Заголовок",
  //         about: "Описание",
  //         logoId: logoId,
  //         imageId: imageId,
  //         videoId: videoId,
  //         color: "HEX", // ЦВЕТ - в формате HEX
  //         serialNumber: 1,
  //     }
  // }

  static requestProjectPropertyCreate = {
    title_ru: 'Заголовок',
    title_en: 'Title',
    description_ru: 'Описание',
    description_en: 'Description',
    serialNumber: 1,
    imageId: 'UUID',
    imagePosition: 'Position', // Тип == позиция ! (Относительно текста)
  };

  static requestProjectStackCreate = {
    stackId: 'UUID',
    serialNumber: 1,
  };
  //На будующее
  // static requestProjectStackCreateId(stackId: string) {
  //     return {
  //         stackId: stackId,
  //         serialNumber: 1,
  //     }
  // }

  static requestProjectStackUpdate = {
    serialNumber: 2,
  };

  static requestProjectsSearch = {
    searchValue: 'Манул',
    show: Object.values(QueryShowType).join('/'),
  };
}
