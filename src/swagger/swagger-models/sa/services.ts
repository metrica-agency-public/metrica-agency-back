import { QueryShowType } from 'utils/constants';
import SAFileModels from './file-db';

export default class SAServicesSwaggerModels {
  //-----RESPONSE-----

  static responseService = {
    id: 'UUID',
    title_ru: 'Заголовок',
    title_en: 'Title',
    about_ru: 'Описание',
    about_en: 'About',
    serialNumber: 1,
    isExist: true,
    image: SAFileModels.resFileDB,
  };

  static responseServiceList = {
    serviceList: [SAServicesSwaggerModels.responseService],
  };

  //-----REQUEST-----

  static requestServiceCreate = {
    title_ru: 'Заголовок',
    title_en: 'Title',
    about_ru: 'Описание',
    about_en: 'About',
    imageId: 'UUID',
    serialNumber: 1,
  };
  static requestServiceSearch = {
    //query
    searchValue: 'Манул',
    show: Object.values(QueryShowType).join('/'),
  };
}
