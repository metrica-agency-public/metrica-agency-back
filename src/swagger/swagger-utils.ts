import BaseSwaggerUtils from 'core/base-swagger-utils';
import { IAdditionalHQ, IApiResponse } from 'core/swagger-classes';
import { Constants } from 'utils/constants';

export default class SwaggerUtils extends BaseSwaggerUtils {
  static SwaggerUtils: IApiResponse;
  static accessTokenHQ(extras?: IAdditionalHQ) {
    return {
      headers: {
        [Constants.ACCESS_TOKEN_HQ]: Constants.ACCESS_TOKEN_HQ,
        ...extras?.headers,
      },
      query: {
        [Constants.ACCESS_TOKEN_HQ + '?']: Constants.ACCESS_TOKEN_HQ,
        ...extras?.query,
      },
    };
  }

  static twoTokens() {
    return this.body200({
      accessToken: 'token1',
      refreshToken: 'token2',
    });
  }

  static messageOk() {
    return {
      message: 'Ok',
    };
  }
}
