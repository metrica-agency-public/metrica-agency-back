import MainPageMetaInfo from 'database/models/final/main-page-meta-info.model';
import Member from 'database/models/final/member.model';
import ProjectProperty from 'database/models/final/project-property.model';
import Project from 'database/models/final/project.model';
import Service from 'database/models/final/service.model';
import Stack from 'database/models/final/stack.model';
import { Lang } from './constants';

export default class LangAttributes {
  private static mapLangField(fieldName: string, el: any, currentLang: Lang) {
    return {
      [fieldName]: el[`${fieldName}_${currentLang}`] as string,
    };
  }

  static langStackList(el: Stack, lang: Lang) {
    return {
      id: el.id,
      ...LangAttributes.mapLangField('title', el, lang),
      ...LangAttributes.mapLangField('description', el, lang),
      image: el.image,
    };
  }

  static langStack(el: Stack, lang: Lang) {
    return {
      id: el.id,
      ...LangAttributes.mapLangField('title', el, lang),
      ...LangAttributes.mapLangField('description', el, lang),
      image: el.image,
    };
  }

  static langMemberStack(el: any, lang: Lang) {
    return {
      ...LangAttributes.langStack(el, lang),
      serialNumber: el['MemberStack'].serialNumber,
    };
  }

  static langProjectStack(el: any, lang: Lang) {
    return {
      ...LangAttributes.langStack(el, lang),
      serialNumber: el['ProjectStack'].serialNumber,
    };
  }

  static langProjectProperty(el: ProjectProperty, lang: Lang) {
    return {
      id: el.id,
      ...LangAttributes.mapLangField('title', el, lang),
      ...LangAttributes.mapLangField('description', el, lang),
      imagePosition: el.imagePosition,
      serialNumber: el.serialNumber,
      image: el.image,
    };
  }

  static langMemberGroup(group: string, lang: Lang) {
    const langVars: any = {
      maintainer_ru: 'Основатели',
      maintainer_en: 'Founders',
      developer_ru: 'Разработка',
      developer_en: 'Developers',
      designer_ru: 'Дизайнеры',
      designer_en: 'Designers',
      analytics_and_qa_ru: 'Аналитика и Тестирование',
      analytics_and_qa_en: 'ANALYTICS / QA',
    };

    const fieldName = group.toLocaleLowerCase();

    return LangAttributes.mapLangField(fieldName, langVars, lang)[fieldName];
  }

  static langMember(el: Member, lang: Lang) {
    return {
      id: el.id,
      ...LangAttributes.mapLangField('firstName', el, lang),
      ...LangAttributes.mapLangField('lastName', el, lang),
      ...LangAttributes.mapLangField('about', el, lang),
      ...LangAttributes.mapLangField('skills', el, lang),
      group: LangAttributes.langMemberGroup(el.group, lang),
      serialNumber: el.serialNumber,
      contactList: el.contactList,
      ...(el.stackList
        ? {
            stackList: el.stackList.map((el) => LangAttributes.langMemberStack(el, lang)),
          }
        : {}),
      fullImage: el.fullImage,
      previewImage: el.previewImage,
    };
  }

  static langProject(el: Project, lang: Lang) {
    return {
      id: el.id,
      ...LangAttributes.mapLangField('title', el, lang),
      ...LangAttributes.mapLangField('about', el, lang),
      color: el.color,
      serialNumber: el.serialNumber,
      logo: el.logo,
      image: el.image,
      video: el.video,
      ...(el.stackList
        ? {
            stackList: el.stackList.map((el) => LangAttributes.langProjectStack(el, lang)),
          }
        : {}),
      propertyList: el.propertyList.map((el) => LangAttributes.langProjectProperty(el, lang)),
    };
  }

  static langMetaInfo(el: MainPageMetaInfo, lang: Lang) {
    return {
      id: el.id,
      ...LangAttributes.mapLangField('title', el, lang),
      ...LangAttributes.mapLangField('description', el, lang),
      serialNumber: el.serialNumber,
    };
  }

  static langService(el: Service, lang: Lang) {
    return {
      id: el.id,
      ...LangAttributes.mapLangField('title', el, lang),
      ...LangAttributes.mapLangField('about', el, lang),
      serialNumber: el.serialNumber,
      image: el.image,
    };
  }
}
