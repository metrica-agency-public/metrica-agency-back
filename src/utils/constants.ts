import TelegramBot from 'node-telegram-bot-api';

class RuntimeConst {
  static TG_BOT: TelegramBot | undefined;
  static TG_BOT_AUTH: TelegramBot | undefined;
  static TOKEN_LIST: string[] = [];
}

class UrlConst extends RuntimeConst {
  static LOCALHOST_MASK: string = `localhost`; //маска localhost
  static LOCAL_URL: string = `http://${UrlConst.LOCALHOST_MASK}`; //Адрес локалХост

  static SWAGGER_DOC_MASK: string = `/swagger/docs`; //маска для swagger docs
  static MEDIA_FOLDER: string = `/media`; //Папка, где хранятся все документы

  static X_CURRENT_LANG_HEADER_NAME: string = 'x-current-lang';
  static X_CURRENT_LANG_QUERY_NAME: string = 'xCurrentLang';
}

export class Constants extends UrlConst {
  static SERVICE_NAME = 'METRICA-AGENCY-BACK';
  static HEADER_ACCESS_TOKEN_LENGTH: number = 128; //Длина токена авторизации
  static ACCESS_TOKEN_HQ: string = 'x-access-token'; //Название заголовка для - accessToken
}

export class ErrorReasons {
  static BAD_REQUEST: string = 'BAD_REQUEST';
  static NOT_FOUND: string = 'NOT_FOUND';
  static UNAUTHORIZED: string = 'UNAUTHORIZED';
  static SERVER_ERROR: string = 'SERVER_ERROR';
}

export enum FileTypes {
  IMAGE = 'IMAGE',
  AUDIO = 'AUDIO',
  VIDEO = 'VIDEO',
  DOCUMENT = 'DOCUMENT',
}

export enum ENVTypes {
  DEV = 'DEV',
  PROD = 'PROD',
  TEST = 'TEST',
}

export enum ContactTypes {
  HH = 'HH',
  TELEGRAM = 'TELEGRAM',
  WHATSAPP = 'WHATSAPP',
  PHONE = 'PHONE',
  GIT = 'GIT',
}

export enum UserRole {
  SA = 'SA',
  COMMON = 'COMMON',
}

export enum ProjectPropertyImagePosition {
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
  UP = 'UP',
  DOWN = 'DOWN',
}

export enum QueryShowType {
  ALL = 'ALL',
  DELETED = 'DELETED',
  EXISTING = 'EXISTING',
}

export enum Lang {
  RU = 'ru',
  ENG = 'en',
}

export enum MemberGroup {
  MAINTAINER = 'MAINTAINER',
  DEVELOPER = 'DEVELOPER',
  DESIGNER = 'DESIGNER',
  ANALYTICS_AND_QA = 'ANALYTICS_AND_QA',
}
