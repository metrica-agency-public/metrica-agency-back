import { plainToClass } from 'class-transformer';
import { ENVTypes } from './constants';

class ProcessENV {
  //Окружение запуска процесса. (МБ - где то понадобится)
  public CUSTOM_ENV: ENVTypes = ENVTypes.DEV;

  //Порты для запуска самого сервера
  public PORT: number = 3000;

  //URL
  public URL: string = '';

  //FRONT URLS
  public FRONT_URL_PROD: string = '';
  public FRONT_URL_DEV: string = '';

  //Всё что касается основной БД
  public DB_URL: string = 'localhost';
  public DB_USERNAME: string = '';
  public DB_PASSWORD: string = '';
  public DB_PORT: number = 5432;
  public DB_NAME: string = '...';

  //Всё что касается бота
  public BOT_TOKEN: string = '';
  public CHAT_ID: string = '';

  //Список login + password для входа в админку
  public ADMIN_LOGIN_LIST: string = '';
  public ADMIN_PASSWORD_LIST: string = '';
}

export default class UtilsENVConfig {
  private static processENV: ProcessENV | null = null;

  static getProcessEnv(): ProcessENV {
    if (this.processENV === null) {
      this.processENV = plainToClass(ProcessENV, process.env);
    }
    return this.processENV;
  }
}
