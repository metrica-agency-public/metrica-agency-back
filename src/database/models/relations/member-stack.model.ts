import { Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import Member from '../final/member.model';
import Stack from '../final/stack.model';

@Table({
  timestamps: true,
})
export default class MemberStack extends Model {
  @ForeignKey(() => Member)
  @Column
  public memberId!: string;

  @ForeignKey(() => Stack)
  @Column
  public stackId!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;
}
