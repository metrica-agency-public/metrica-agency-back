import { Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import Project from '../final/project.model';
import Stack from '../final/stack.model';

@Table({
  timestamps: true,
})
export default class ProjectStack extends Model {
  @ForeignKey(() => Project)
  @Column
  public projectId!: string;

  @ForeignKey(() => Stack)
  @Column
  public stackId!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;
}
