import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import { ContactTypes } from 'utils/constants';
import Member from './member.model';

@Table({
  timestamps: true,
})
export default class Contact extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.ENUM({ values: Object.values(ContactTypes) }),
  })
  public type!: string;

  @Column({
    type: DataType.STRING,
  })
  public value!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;

  @BelongsTo(() => Member, 'memberId')
  public member!: Member;

  @ForeignKey(() => Member)
  public memberId!: string;
}
