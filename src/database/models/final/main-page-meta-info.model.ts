import { Column, DataType, Model, Table } from 'sequelize-typescript';

@Table({
  timestamps: true,
})
export default class MainPageMetaInfo extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
  })
  public title_ru!: string;
  
  @Column({
    type: DataType.STRING,
  })
  public title_en!: string;

  @Column({
    type: DataType.STRING,
  })
  public description_ru!: string;

  @Column({
    type: DataType.STRING,
  })
  public description_en!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;
}
