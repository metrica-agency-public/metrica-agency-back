import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import MemberStack from '../relations/member-stack.model';
import ProjectStack from '../relations/project-stack.model';
import FileDB from './file-db.model';
import Member from './member.model';
import Project from './project.model';

@Table({
  timestamps: true,
})
export default class Stack extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public description_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public description_en!: string;

  @BelongsToMany(() => Project, () => ProjectStack)
  public projectList!: Project[];

  @BelongsToMany(() => Member, () => MemberStack)
  public memberList!: Member[];

  @ForeignKey(() => FileDB)
  public imageId!: string;

  @BelongsTo(() => FileDB, 'imageId')
  public image!: FileDB;
}
