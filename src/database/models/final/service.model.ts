import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import FileDB from './file-db.model';

@Table({
  timestamps: true,
})
export default class Service extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public about_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public about_en!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: true,
  })
  public isExist!: boolean;

  @ForeignKey(() => FileDB)
  public imageId!: string;

  @BelongsTo(() => FileDB, 'imageId')
  public image!: FileDB;
}
