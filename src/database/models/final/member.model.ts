import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript';
import { MemberGroup } from 'utils/constants';
import MemberStack from '../relations/member-stack.model';
import Contact from './contact.model';
import FileDB from './file-db.model';
import Stack from './stack.model';

@Table({
  timestamps: true,
})
export default class Member extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public firstName_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public firstName_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public lastName_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public lastName_en!: string;

  @Column({
    type: DataType.TEXT,
    allowNull: false,
  })
  public about_ru!: string;

  @Column({
    type: DataType.TEXT,
    allowNull: false,
  })
  public about_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public skills_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public skills_en!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: true,
  })
  public isExist!: boolean;

  @Column({
    type: DataType.ENUM({ values: Object.values(MemberGroup) }),
  })
  public group!: string;

  @BelongsToMany(() => Stack, () => MemberStack)
  public stackList!: Stack[];

  @HasMany(() => Contact, { onDelete: 'CASCADE' })
  public contactList!: Contact[];

  @ForeignKey(() => FileDB)
  public previewImageId!: string;

  @BelongsTo(() => FileDB, 'previewImageId')
  public previewImage!: FileDB;

  @ForeignKey(() => FileDB)
  public fullImageId!: string;

  @BelongsTo(() => FileDB, 'fullImageId')
  public fullImage!: FileDB;
}
