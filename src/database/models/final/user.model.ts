import { Column, DataType, HasMany, Index, Model, Table } from 'sequelize-typescript';
import { UserRole } from 'utils/constants';
import Token from './token.model';

@Table({
  timestamps: true,
})
export default class User extends Model {
  @Index
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public email!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public password!: string;

  @Column({
    type: DataType.ENUM({ values: Object.values(UserRole) }),
    defaultValue: UserRole.COMMON,
  })
  public role!: string;

  @HasMany(() => Token)
  public tokenList!: Token[];
}
