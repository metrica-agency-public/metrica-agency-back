import { BelongsTo, Column, DataType, ForeignKey, Model, Table } from 'sequelize-typescript';
import { ProjectPropertyImagePosition } from 'utils/constants';
import FileDB from './file-db.model';
import Project from './project.model';

@Table({
  timestamps: true,
})
export default class ProjectProperty extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public description_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public description_ru!: string;

  @Column({
    type: DataType.ENUM({ values: Object.values(ProjectPropertyImagePosition) }),
  })
  public imagePosition!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;

  @ForeignKey(() => Project)
  public projectId!: string;

  @BelongsTo(() => Project, 'projectId')
  public project!: Project;

  @ForeignKey(() => FileDB)
  public imageId!: string;

  @BelongsTo(() => FileDB, 'imageId')
  public image!: FileDB;
}
