import { BelongsTo, BelongsToMany, Column, DataType, ForeignKey, HasMany, Model, Table } from 'sequelize-typescript';
import ProjectStack from '../relations/project-stack.model';
import FileDB from './file-db.model';
import ProjectProperty from './project-property.model';
import Stack from './stack.model';

@Table({
  timestamps: true,
})
export default class Project extends Model {
  @Column({
    defaultValue: DataType.UUIDV4,
    type: DataType.UUID,
    primaryKey: true,
  })
  public id!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public title_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public about_ru!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public about_en!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  public color!: string;

  @Column({
    type: DataType.INTEGER,
    defaultValue: 0,
  })
  public serialNumber!: number;

  @Column({
    type: DataType.BOOLEAN,
    defaultValue: true,
  })
  public isExist!: boolean;

  @BelongsToMany(() => Stack, () => ProjectStack)
  public stackList!: Stack[];

  @HasMany(() => ProjectProperty, { onDelete: 'CASCADE' })
  public propertyList!: ProjectProperty[];

  @ForeignKey(() => FileDB)
  public logoId!: string;

  @BelongsTo(() => FileDB, 'logoId')
  public logo!: FileDB;

  @ForeignKey(() => FileDB)
  public imageId!: string;

  @BelongsTo(() => FileDB, 'imageId')
  public image!: FileDB;

  @ForeignKey(() => FileDB)
  public videoId!: string;

  @BelongsTo(() => FileDB, 'videoId')
  public video!: FileDB;
}
